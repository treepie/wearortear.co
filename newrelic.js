/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
var env = process.env.NODE_ENV || 'development';
var filePath = 'log/newrelic_agent.log';
exports.config = {
    /**
     * Array of application names.
     */
    app_name: ['Express ImageServer - ' + env],
    /**
     * Your New Relic license key.
     */
    license_key: 'c1b49615b2eac598d6a860a9facc5387628866c5',
    logging: {
        /**
         * Level at which to log. 'trace' is most useful to New Relic when diagnosing
         * issues with the agent, 'info' and higher will impose the least overhead on
         * production applications.
         */
        level: 'info',
        filepath: filePath
    }
};
