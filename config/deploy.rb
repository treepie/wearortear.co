#config/deploy.rb

#load other tasks
require 'capistrano/nginx/tasks'

#start configuration from here
set :application, "wearortear_web"

# see https://help.github.com/articles/deploying-with-capistrano
# on how to deploy with github and capistrano

#repository & security configurations
set :repository, "git@bitbucket.org:treepie/wearortear.co.git"
ssh_options[:forward_agent] = true
set :scm, :git                                      #capper default
set :keep_releases, 5                               #capper default
set :deploy_via, :remote_cache                      #capper default

#User configurations
set :use_sudo, false
set :sudo_user, 'root'                            #capper default

# your log folder to share between different releases
# you can add more folders here...
# set :symlinks, {"log" => "log"}
set :shared_children, %w{node_modules}
set :symlinks, {"log" => "log","node_modules"=>"node_modules"}
before 'deploy:reverted', 'npm:install'

set :listen_port, nil
set :npm_cmd, 'npm --unsafe-perm'
set :forever_cmd, "./node_modules/.bin/forever"
logger.level = Logger::MAX_LEVEL         #use the forever that is installed along with the app

###Environment specific configurations

desc "production stage"
task :production do
# skip using nave on production server
  set :use_nave, false
  set :application, "wearortear_web"
  set :server_name, 'wearortear.co'
  set :branch, 'production'
  set :user, 'root'
  set :deploy_to, "/srv/apps/#{application}"
  set :main_js, "#{deploy_to}/current/app.js"
  set :node_env, 'production'
  set :app_port, '5001'
  set :default_environment, {
    'PORT' => '5001'
  }
  set :forever_cmd, "./node_modules/.bin/forever -o #{deploy_to}/current/log/application.log -e #{deploy_to}/current/log/error.log -a"
  server 'wearortear.co', :app
end

desc "staging stage"
task :staging do
  set :use_nave, false
  set :application, "wearortear_web_stage"
  set :server_name, 'web.stage.wearortear.co'
  set :branch, 'staging'
  set :user, 'root'
  set :deploy_to, "/srv/apps/#{application}"
  set :main_js, "#{deploy_to}/current/app.js"
  set :node_env, 'staging'
  set :app_port, '5003'
  set :default_environment, {
    'PORT' => '5003'
  }
  set :forever_cmd, "./node_modules/.bin/forever -o #{deploy_to}/current/log/application.log -e #{deploy_to}/current/log/error.log -a"
  server 'web.stage.wearortear.co', :app
end

desc "testing stage"
task :testing do
  set :use_nave, false
  set :application, "wearortear_web_test"
  set :server_name, 'web.test.wearortear.co'
  set :branch, 'master'
  set :user, 'root'
  set :deploy_to, "/srv/apps/#{application}"
  set :main_js, "#{deploy_to}/current/app.js"
  set :node_env, 'test'
  set :app_port, '5002'
  set :default_environment, {
    'PORT' => '5002'
  }
  set :forever_cmd, "./node_modules/.bin/forever -o #{deploy_to}/current/log/application.log -e #{deploy_to}/current/log/error.log -a"
  server 'web.test.wearortear.co', :app
end


desc "demo stage"
task :demo do
  set :use_nave, false
  set :application, "wearortear_web_demo"
  set :server_name, 'web.demo.wearortear.co'
  set :branch, 'demo'
  set :user, 'root'
  set :deploy_to, "/srv/apps/#{application}"
  set :main_js, "#{deploy_to}/current/app.js"
  set :node_env, 'demo'
  set :app_port, '5004'
  set :default_environment, {
    'PORT' => '5004'
  }
  set :forever_cmd, "./node_modules/.bin/forever -o #{deploy_to}/current/log/application.log -e #{deploy_to}/current/log/error.log -a"
  server 'web.demo.wearortear.co', :app
end

desc "tail the application logfile"
task :log do
  log = "#{application_dir}/current/log/application.log"
  run "tail -f #{log}"
end

after "deploy:setup", "nginx:setup", "nginx:reload"