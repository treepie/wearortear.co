module.exports = function(app){

	//home route
	var home = require('../app/controllers/home');
    var subscription = require('../app/controllers/subscription');
    app.get('/', home.index);
    app.get('/dl',function(req,res) {res.redirect('http://ad.apps.fm/bYKYNc4Cvz8KwO8bLtyveV5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mfwj8Z0IhQGNT4mTcfH2nhqSNXDNE9HwMvm89mEgfX6cS');});
    app.get('/signup-confirm', subscription.signupconfirm);
    app.get('/subscription-done', subscription.subscriptiondone);
    app.get('/app', function(req,res) {res.redirect('http://ad.apps.fm/T-DNQL2Mo0aT6PcKKdwYhl5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mf0PfAfXw_O_bczN6Odfqyyaz6nC_zKrMGrC7IkNiN8fq');});
    app.get('/bbm', function(req,res) {res.redirect('http://ad.apps.fm/tlyomHUwyMLk6kRfbUuV1V5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mf-zVzgszg-RZ36kTYMWIUXULq26R3uZ-hURD3Immx5uN');});
    app.get('/fb', function(req,res) {res.redirect('http://ad.apps.fm/6DmNMLoONUUs5rGsxn1bxV5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mf6xdUKqbQMoLJmvq5XsoPKbVj6MZNJLh6N6m-09S5VyC');});
    app.get('/wa', function(req,res) {res.redirect('http://ad.apps.fm/W0kKB-zZ6Tr-2PUnsvYf615KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mf2Xfn2fxwtqTWP6TaVaEuAkqhCQd0HYU9e3YjDJUZBF8');});
    app.get('/email', function(req,res) {res.redirect('http://ad.apps.fm/sqfcipaZdzcybOc5YMvopl5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0MfzK_NZteC4c-CHwbSAtdQSk4MrHrJPxFMXNgbKzdqNGq');});
    app.get('/pin', function(req,res) {res.redirect('http://ad.apps.fm/RXfwKVps-Mf240gv3Cx_nV5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mf5qtteeZvVAQsLJaofExwWpFkbxXticbAu1fiqkb8MJu');});
    app.get('/twt', function(req,res) {res.redirect('http://ad.apps.fm/8rznOScX9_JF6eTueTCuV15KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mf1w7M9V8DoEe2z84oLrey1h5XfBSbSbNmx95ZB5gVaiL');});
    app.get('/admob', function(req,res) {res.redirect('http://ad.apps.fm/VTzCYpJilsekWStQFOZaBF5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mf4WenhxQG4LjstpafKC0tDrddwbCx5I-mntJ8MRjZ_wu');});
    app.get('/stfn', function(req,res) {res.redirect('http://ad.apps.fm/at8X5bPCteEU4i6gQnB3wF5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0MfyRZsp9Pp4BWXwEHmt2dEVQSKWo8btJVKbnzMA3soIZDfwcXxa_9owEsDgyHAy7y4Q');});
    app.get('/wa-fddi', function(req,res) {res.redirect('http://ad.apps.fm/BzU2LB5fi9-oNvdKlta2aF5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0Mfx-oqTn0DmU75aS9zQbzwUlxwObfGUbC_e402OML1gAY');});
    app.get('/wa-rit', function(req,res) {res.redirect('http://ad.apps.fm/_9PpAxjcYd3Ruk6RRP5MbF5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfH5OdcLS2Zr8-RS4guH0MfwxrNht8LTRDz8lQ9hrfNvaqKQJZpjEjO1oCod-Er1g_wofMNeMTlYLWlVhcZb78Im22JYOrvk9EEFN4yIuDA4A');});
};