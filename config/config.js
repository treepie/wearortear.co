var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
    development: {
        root: rootPath,
        app: {
            name: 'wearortear-web'
        },
        port: 5000,
        db: 'mongodb://localhost/wearortear-web-development',
        cacheDir: '/var/tmp/imagecache/',
        accessKeyId: process.env.AWS_ACCESS_KEY || 'AKIAJQBQXI25N7JC4JGQ',
        secretAccessKey: process.env.AWS_SECRET_KEY || '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',
        bucket: 'lukup-images',
        allowedDomains: '*'
    },

    test: {
        root: rootPath,
        app: {
            name: 'wearortear-web'
        },
        port: process.env.PORT || 5002,
        db: 'mongodb://localhost/wearortear-web-test',
        cacheDir: '/var/tmp/imagecache/',
        accessKeyId: process.env.AWS_ACCESS_KEY || 'AKIAJQBQXI25N7JC4JGQ',
        secretAccessKey: process.env.AWS_SECRET_KEY || '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',
        bucket: 'lukup-images',
        allowedDomains: '*'
    },
    demo: {
        root: rootPath,
        app: {
            name: 'wearortear-web'
        },
        port: process.env.PORT || 5004,
        db: 'mongodb://localhost/wearortear-web-demo',
        cacheDir: '/var/tmp/imagecache/',
        accessKeyId: process.env.AWS_ACCESS_KEY || 'AKIAJQBQXI25N7JC4JGQ',
        secretAccessKey: process.env.AWS_SECRET_KEY || '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',
        bucket: 'lukup-images',
        allowedDomains: '*'
    },
    staging: {
        root: rootPath,
        app: {
            name: 'wearortear-web'
        },
        port: process.env.PORT || 5003,
        db: 'mongodb://localhost/wearortear-web-staging',
        cacheDir: '/var/tmp/imagecache/',
        accessKeyId: process.env.AWS_ACCESS_KEY || 'AKIAJQBQXI25N7JC4JGQ',
        secretAccessKey: process.env.AWS_SECRET_KEY || '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',
        bucket: 'lukup-images',
        allowedDomains: '*'
    },

    production: {
        root: rootPath,
        app: {
            name: 'wearortear-web'
        },
        port: process.env.PORT || 5001,
        db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://localhost/wearortear-web-production',
        cacheDir: '/var/tmp/imagecache/',
        accessKeyId: process.env.AWS_ACCESS_KEY || 'AKIAJQBQXI25N7JC4JGQ',
        secretAccessKey: process.env.AWS_SECRET_KEY || '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',
        bucket: 'lukup-images',
        allowedDomains: '*'
    }
};

module.exports = config[env];
