var express = require('express'),
    config = require('./config');

var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', config.allowedDomains);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');

    next();
}

module.exports = function (app, config) {
    app.configure(function () {
        app.use(express.compress());
        app.use(allowCrossDomain);
        app.use(express.static(config.root + '/public'));
        app.set('port', config.port);
        app.set('views', config.root + '/app/views');
        app.set('view engine', 'ejs');
        app.use(express.favicon(config.root + '/public/images/favicon.png'));
        app.use(express.logger('dev'));
        app.use(express.bodyParser());
        app.use(express.methodOverride());
        app.use(app.router);
        app.use(function (req, res) {
            res.status(404).render('404', { title: '404' });
        });
    });
};
