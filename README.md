[TOC]
#Developer Rampup
This section will help new developers get ready for contributing to the Image Server

##Technologies
Image Server is written in Nodejs and we use the Express framework. We use MongoDB as datastore. Follow the links below to learn more about the technologies used.
* [Node.js](http://www.nodejs.org/) - Server Side Javascript runner which uses V8 Engine
* [Express](http://expressjs.com/) - A web application framework for NodeJS
* [MongoDB](http://www.mongodb.org/) - A NoSQL database

##Developer Setup
In order to setup the project in your local environment, follow the instructions below. If you had already installed any of these components and the versions match, you should be good to skip that section.
###1. Install Nodejs & NPM
[Download & Install Node.js](http://www.nodejs.org/download/) and the npm package manager, if you encounter any problems, you can also use this [Github Gist](https://gist.github.com/isaacs/579814) to install Node.js.
###2. Install MongoDB
[Download & Install MongoDB](http://www.mongodb.org/downloads), and make sure it's running on the default port (27017).

###3. Install Grunt
You're going to use the [Grunt Task Runner](http://gruntjs.com/) to automate your development process, in order to install it make sure you've installed Node.js and npm, then install grunt globally using npm:
```
$ sudo npm install -g grunt-cli
```
###4. Clone The Bitbucket Repository
You can use Git to directly clone the Meaver API repository. If your system doesn't have git, [install git first](http://git-scm.com/book/en/Getting-Started-Installing-Git).
```
$ git clone git@bitbucket.org:treepie/wearortear.co.git
```
This will clone the latest version of the Meaver API repository. 

###5. Install node dependencies

The first thing you should do is install the Node.js dependencies. To install Node.js dependencies you're going to use npm again, in the application folder (meaver-api) run this in the command-line:
```
$ npm install
```
This command does a few things:
* First it will install the dependencies needed for the application to run.
* If you're running in a development environment, it will then also install development dependencies needed for testing and running your application.

###6. Running the Server
After the install process is over, you'll be able to start the server using Grunt, just run grunt default task:
```
$ grunt
```
Your application should run on the 4000 port so in your browser just go to [http://localhost:4000](http://localhost:4000)
                            
That's it! your application should be running by now, to proceed with your development check the other sections in this documentation. 
If you encounter any problem try the Troubleshooting section.

##References

* MongoDB - Go through [MongoDB Official Website](http://mongodb.org/) and proceed to their [Official Manual](http://docs.mongodb.org/manual/), which should help you understand NoSQL and MongoDB better.
* Express - The best way to understand express is through its [Official Website](http://expressjs.com/), particularly [The Express Guide](http://expressjs.com/guide.html); you can also go through this [StackOverflow Thread](http://stackoverflow.com/questions/8144214/learning-express-for-node-js) for more resources.
* Node.js - Start by going through [Node.js Official Website](http://nodejs.org/) and this [StackOverflow Thread](http://stackoverflow.com/questions/2353818/how-do-i-get-started-with-node-js), which should get you going with the Node.js platform in no time.

## Authors
@negoel