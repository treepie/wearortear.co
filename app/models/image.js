// Example model

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ImageSchema = new Schema({
    storage: {
        type: Schema.Types.Mixed
    },
    imageId: {
        type: String,
        unique: true
    },
    name: {
        type: String
    },
    mimeType: {
        type:String
    }
});


mongoose.model('Image', ImageSchema);
