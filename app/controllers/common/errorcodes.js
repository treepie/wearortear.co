'use strict';

var errormessages = {};

//errorcodes
exports.SUCCESS = 200;
exports.ERROR = 400;
exports.UNKNOWN_FIELD = 11000;
errormessages[exports.SUCCESS] = 'Success';
errormessages[exports.ERROR] = 'Some error occurred.';
errormessages[exports.UNKNOWN_FIELD] = 'Unknown field in the input';


//DB related error codes
exports.DB_READ_ERROR = 12000;
exports.DB_WRITE_ERROR = 12001;
errormessages[exports.DB_READ_ERROR] = 'Error while reading data from DB';
errormessages[exports.DB_WRITE_ERROR] = 'Error while writing data to DB';

//Image related error codes
exports.INVALID_MIME_TYPE = 13000 ;
exports.INVALID_IMAGE_ID = 13001 ;
exports.CROP_FAILED = 13002 ;
exports.RESIZE_FAILED = 13002 ;
errormessages[exports.INVALID_MIME_TYPE] = 'Invalid Mime Type';
errormessages[exports.INVALID_IMAGE_ID] = 'Invalid Image Id';
errormessages[exports.CROP_FAILED] = 'Error while cropping.';
errormessages[exports.RESIZE_FAILED] = 'Error while resizing.';

//File System Errors
exports.FILE_RENAME_FAILED = 14000;
exports.FILE_NOT_FOUND = 14001;
errormessages[exports.FILE_RENAME_FAILED] = 'File Rename Failed';
errormessages[exports.FILE_NOT_FOUND] = 'File Not Found';

//S3 related errors
exports.WRITE_TO_S3_FAILED = 15000;
errormessages[exports.WRITE_TO_S3_FAILED] = 'Write to S3 failed';

//GridFS related errors
exports.WRITE_TO_GFS_FAILED = 16000;
errormessages[exports.WRITE_TO_GFS_FAILED] = 'Write to Grid FS failed';

exports.READ_FROM_GFS_FAILED = 16001;
errormessages[exports.READ_FROM_GFS_FAILED] = 'Read from Grid FS failed';

//utility function
exports.getErrorMessage = function (errorId) {
    return errormessages[errorId];
};

