'use strict';
var errorcodes = require('../errorcodes');

exports.getErrorResponse = function (errorcode) {
    var err = {
        'status': 'error',
        'code':errorcode,
        'message': errorcodes.getErrorMessage(errorcode)
    };
    return err;
};

exports.getFailResponse = function (data) {
    var fail = {
        'status': 'fail',
        'data':data
    };
    return fail;
};

exports.getSuccessResponse = function (data) {
    var success = {
        'status': 'success',
        'data':data
    };
    return success;
};