var mongoose = require('mongoose'),
    Article = mongoose.model('Article');

exports.signupconfirm = function(req, res){
    Article.find(function(err, articles){
        if(err) throw new Error(err);
        res.render('subscription/signupconfirm', {
            title: 'Confirm Signup'
        });
    });
};

exports.subscriptiondone = function(req, res){
    Article.find(function(err, articles){
        if(err) throw new Error(err);
        res.render('subscription/subscriptiondone', {
            title: 'Subscription Done'
        });
    });
};